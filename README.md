# Filmopolis

This project was generated with [Angular CLI] version 16.0.5.

## Install the repo

Clone the repo in your desire folder then run `npm i` to install dependencies

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Project idea

Welcome to Filmopolis!

Filmopolis is a website that utilizes the IMDb database to provide users with comprehensive information about films, actors, and related details. The primary goal of this project is to create a user-friendly interface where users can easily search for films and explore their associated actors, directors, genres, release dates, and other relevant information.

Key Features:

1. Film Search: Users can search for films by title. The search functionality provides quick and accurate results, making it convenient for users to find the films they are interested in.

2. Film Details: Upon selecting a film, users can access detailed information about the film, including its synopsis, cast members, crew, and other relevant details. This provides users with a comprehensive overview of the film and its key contributors.

3. User-Friendly Interface: IMDb Film Explorer prioritizes an intuitive and user-friendly interface, ensuring that users can navigate the website seamlessly and access the desired information without any hassle.


