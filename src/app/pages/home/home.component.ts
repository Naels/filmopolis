import { Component, OnInit } from '@angular/core';
import { MovieApiServiceService } from 'src/app/service/movie-api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service:MovieApiServiceService){  }

  bannerResults   : any = []
  trendingResults : any = []

  actionMovieResults : any=[]
  adventureMovieResults : any=[]
  animationMovieResults : any=[]
  comedyMovieResults : any=[]
  documentaryMovieResults : any=[]
  scifiMovieResults : any=[]
  thrillerMovieResults : any=[]


  ngOnInit(): void{
    this.bannerData()
    this.trendingData()
    this.actionMovie()
    this.adventureMovie()
    this.animationMovie()
    this.comedyMovie()
    this.documentaryMovie()
    this.scifiMovie()
    this.thrillerMovie()
  }

  bannerData(){
    this.service.bannerApiData().subscribe((res)=>{
      this.bannerResults = res.results
    })
  }

  trendingData(){
    this.service.trendingMovieApiData().subscribe((res)=>{
      this.trendingResults = res.results
    })
  }

  actionMovie(){
    this.service.fetchActionMovies().subscribe((res)=>{
      this.actionMovieResults = res.results
    })
  }

  adventureMovie(){
    this.service.fetchAdventureMovies().subscribe((res)=>{
      this.adventureMovieResults = res.results
    })
  }

  animationMovie(){
    this.service.fetchAnimationMovies().subscribe((res)=>{
      this.animationMovieResults = res.results
    })
  }

  comedyMovie(){
    this.service.fetchComedyMovies().subscribe((res)=>{
      this.comedyMovieResults = res.results
    })
  }

  documentaryMovie(){
    this.service.fetchDocumentaryMovies().subscribe((res)=>{
      this.documentaryMovieResults = res.results
    })
  }

  scifiMovie(){
    this.service.fetchSciFiMovies().subscribe((res)=>{
      this.scifiMovieResults = res.results
    })
  }

  thrillerMovie(){
    this.service.fetchThrillerMovies().subscribe((res)=>{
      this.thrillerMovieResults = res.results
    })
  }

}
